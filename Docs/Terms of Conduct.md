**Terms of Conduct**


1.  Being (more than 10 minutes) late in a meeting:

    *  1st time: You “take minutes” for the next meeting

    *  2nd time: You Tikkie a TU Delft coffee to everyone (42 cents)

    *  3th time: notify the TA and possibly the coach about the situation.

2. Missing the internal deadline

    *  1st time: Doing the static analysis for everyone one time(check style, pmd, etc.)

    *  2nd time:  Doing the static analysis for everyone three times(check style, pmd, etc.)

    *  3rd time: notify the TA and possibly the coach about the situation.
3. We agree to follow the team social ethics, by respecting our team members and their work, ideas and suggestions and state our opinions in a logical and informative manner, with proper arguments and openness towards different mentalities regarding any subject.

4. We agree to communicate with the group when we finish elements of the project so that the work is only done once and everyone can look into it.
5. We agree to treat the sensoglove equipment as your own, e.g. not destroying the haptic gloves on purpose.

Signatures:


Nikos Mertzanis

(n.mertzanis@student.tudelft.nl)

Neha Kalia
(N.Kalia@student.tudelft.nl)

Jonathan Rozen
(J.Rozen@student.tudelft.nl)

Nils van Veen
(N.vanVeen-3@student.tudelft.nl)

Simran Karnani
(s.r.karnani@student.tudelft.nl)

