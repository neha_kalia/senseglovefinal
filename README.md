*SenseGlove OpenVR Integration*

**Running the code**

1. Open the repository as a folder in Visual Studio

2. Go to Build -> Install senseglovevrCI. 

3. This should be the build output up in the build
	 -- Installing: ../senseglovevr/out/install/x64-Debug/drivers/sample/bin/win64/driver_sample.dll
	 -- Up-to-date: ../senseglovevr/out/install/x64-Debug/drivers/sample/resources/settings/default.vrsettings
     -- Up-to-date: ../senseglovevr/out/install/x64-Debug/drivers/sample/bin/win64/SGConnect.dll
     -- Up-to-date: ../senseglovevr/out/install/x64-Debug/drivers/sample/bin/win64/SGConnect.lib
     -- Up-to-date: ../senseglovevr/out/install/x64-Debug/drivers/sample/bin/win64/SGCore.lib
     -- Up-to-date: ../senseglovevr/out/install/x64-Debug/drivers/sample/bin/win64/SGCore.dll

4. Go to the directory containing the sample folder

5. Copy the folder "sample" into the folder "\steamapps\common\SteamVR

6. Install the SteamVR plug-in for Unity according to the steps mentioned in https://valvesoftware.github.io/steamvr_unity_plugin/articles/Quickstart.html

7. You can either create your own project, or use the ready-to-use InteractionExample scene.

8. Run the scene.

9. Go wild.


**Looking at the code**

1. The lib directory contains the external subdmodules our project depends on, namely OpenVR and the SenseGlove API

2. The code for generating the driver can be found in the OpenVR/samples/driver_sample directory.

	* The csampledriver.cpp, cwatchdogdriver.cpp, driver_sample.cpp and drivelog.cpp have been taken from the sample OpenVR driver 
	without modification, since they relate to creating the required the dlls and simulating a headset.

	* The code written by us for this project can be found in SenseGloveConnect.cpp, cserverdriver_sample.cpp and
	csamplecontrollerdriver.cpp

			* Support is added for users without a glove. A simulated right handed glove appears (when no gloves are connected)
			which switches between four glove poses to give the user an idea on how the product works.

3. The TestSuite directory contains our code to generate serialized data from the senseglove, for the purpose of simulating 
a glove and testing our code.




** Movement and hardcoded behaviour of glove feedback **
* Pressing + enables force feedback
* Pressing - disables force feedback
* Pressing * enables vibrations
* Pressing / disables vibrations
* Pressing Left Shift + Right Shift to toggle the wrist rotations calculated by the IMU



** Moving the gloves and HMD without tracking and/or having the HMD plugged in **

* To walk use the arrow keys / Or when Num Lock is turned on you can use the keypad.
* To move the left controller, you can use the following keys: W,A,S,D
* To change the height of the left controller, you can use: Q,E
* To reset the location of the left controller, use: R
* To rest the location of the right contorller, use: p
* Right controller uses the following keys: **** (inverted with regard to left keyboard layout) , e.g. I,J,K,L
* Rotating the HMD (NumLock turned off) : 2,4,6,8 / 1,3
* Resetting your location (Num Lock turned off) : 9


Code References:

* Template driver : https://github.com/r57zone/OpenVR-driver-for-DIY
* Template joint position array : https://github.com/spayne/soft_knuckles/blob/master/soft_knuckles_device.cpp


