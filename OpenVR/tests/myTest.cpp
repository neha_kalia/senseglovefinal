#include <string>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../headers/db.h"

using::testing::Return;

//sample test : exanple of how to write tests in gtest
class SampleTests : public ::testing::Test
{
};

TEST_F(SampleTests, simple)
{
    EXPECT_EQ(1, 1);
}


/* Database methods and tests are just an example of how
* to work with gmocks.
*/

//template <class DatabaseConnect>
//class  MyDatabase {
//public:
//    MyDatabase(Database::DatabaseConnect& _dbc) : dBC(_dbc) {}
//    int Init(std::string username, std::string password);
//};

class DatabaseInterface {
 public:
  virtual bool login(std::string username, std::string password) = 0;
};

class db : public DatabaseInterface {
public:
    virtual bool login(std::string username, std::string password) {
        return Database::DatabaseConnect::login(username, password);
    }
};

class MockDB  {
public:
    MOCK_METHOD(int, fetchRecord, ());
    MOCK_METHOD(bool, logout, (std::string username));
    MOCK_METHOD(bool, login, (std::string username, std::string password));
};



//example test using gmocks.
TEST_F(SampleTests, loginTest) {
    //Arrange
    MockDB mdb;
 
    MyDatabase<MockDB> db(mdb);
    EXPECT_CALL(mdb, login("Terminator", "I'm back")).Times(1).WillOnce(Return(true));

    //Act
    int retValue = db.Init("Terminator", "I'm back");

    //Assert
    EXPECT_EQ(retValue, 1);

}
