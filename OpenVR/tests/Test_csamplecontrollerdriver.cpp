#include <string>
#include <gtest/gtest.h>
#include "../headers/csamplecontrollerdriver.h"
#include <gmock/gmock.h>
#include "MockSenseGlove.h"
#include "../samples/driver_sample/csamplecontrollerdriver.cpp"

#if defined(_WINDOWS)
#include <windows.h>
#include <winuser.h>
#endif



using ::testing::AtLeast;
using ::testing::_;
using::testing::Return;

using namespace std;


/**
* Binary == method for VRBoneTransform_t.
* An equals method is needed for the expect equals.
*/
namespace vr {

    bool operator ==(VRBoneTransform_t x, VRBoneTransform_t y){
        return (x.orientation.w == y.orientation.w && x.orientation.y == y.orientation.y && x.orientation.z == y.orientation.z && x.orientation.x == y.orientation.x
        && x.position.v[4] == y.position.v[4]);
	}
}

//setting up the test fixture
struct ControllDriver : public ::testing::Test
{
    CSampleControllerDriver<SGCore::SG::SenseGlove>* controllerDriver;
    CSampleControllerDriver<MockSenseGlove>* mockControllerDriver;
    static const int N_BONES = 31;

    //A default open hand pose (left hand)
    vr::VRBoneTransform_t left_open_hand_pose[N_BONES] = {
    { { 0.000000f,  0.000000f,  0.000000f,  1.000000f}, { 1.000000f, -0.000000f, -0.000000f,  0.000000f} },
    { {-0.034038f,  0.036503f,  0.164722f,  1.000000f}, {-0.055147f, -0.078608f, -0.920279f,  0.379296f} },
    { {-0.012083f,  0.028070f,  0.025050f,  1.000000f}, { 0.464112f,  0.567418f,  0.272106f,  0.623374f} },
    { { 0.040406f,  0.000000f, -0.000000f,  1.000000f}, { 0.994838f,  0.082939f,  0.019454f,  0.055130f} },
    { { 0.032517f,  0.000000f,  0.000000f,  1.000000f}, { 0.974793f, -0.003213f,  0.021867f, -0.222015f} },
    { { 0.030464f, -0.000000f, -0.000000f,  1.000000f}, { 1.000000f, -0.000000f, -0.000000f,  0.000000f} },
    { { 0.000632f,  0.026866f,  0.015002f,  1.000000f}, { 0.644251f,  0.421979f, -0.478202f,  0.422133f} },
    { { 0.074204f, -0.005002f,  0.000234f,  1.000000f}, { 0.995332f,  0.007007f, -0.039124f,  0.087949f} },
    { { 0.043930f, -0.000000f, -0.000000f,  1.000000f}, { 0.997891f,  0.045808f,  0.002142f, -0.045943f} },
    { { 0.028695f,  0.000000f,  0.000000f,  1.000000f}, { 0.999649f,  0.001850f, -0.022782f, -0.013409f} },
    { { 0.022821f,  0.000000f, -0.000000f,  1.000000f}, { 1.000000f, -0.000000f,  0.000000f, -0.000000f} },
    { { 0.002177f,  0.007120f,  0.016319f,  1.000000f}, { 0.546723f,  0.541276f, -0.442520f,  0.460749f} },
    { { 0.070953f,  0.000779f,  0.000997f,  1.000000f}, { 0.980294f, -0.167261f, -0.078959f,  0.069368f} },
    { { 0.043108f,  0.000000f,  0.000000f,  1.000000f}, { 0.997947f,  0.018493f,  0.013192f,  0.059886f} },
    { { 0.033266f,  0.000000f,  0.000000f,  1.000000f}, { 0.997394f, -0.003328f, -0.028225f, -0.066315f} },
    { { 0.025892f, -0.000000f,  0.000000f,  1.000000f}, { 0.999195f, -0.000000f,  0.000000f,  0.040126f} },
    { { 0.000513f, -0.006545f,  0.016348f,  1.000000f}, { 0.516692f,  0.550143f, -0.495548f,  0.429888f} },
    { { 0.065876f,  0.001786f,  0.000693f,  1.000000f}, { 0.990420f, -0.058696f, -0.101820f,  0.072495f} },
    { { 0.040697f,  0.000000f,  0.000000f,  1.000000f}, { 0.999545f, -0.002240f,  0.000004f,  0.030081f} },
    { { 0.028747f, -0.000000f, -0.000000f,  1.000000f}, { 0.999102f, -0.000721f, -0.012693f,  0.040420f} },
    { { 0.022430f, -0.000000f,  0.000000f,  1.000000f}, { 1.000000f,  0.000000f,  0.000000f,  0.000000f} },
    { {-0.002478f, -0.018981f,  0.015214f,  1.000000f}, { 0.526918f,  0.523940f, -0.584025f,  0.326740f} },
    { { 0.062878f,  0.002844f,  0.000332f,  1.000000f}, { 0.986609f, -0.059615f, -0.135163f,  0.069132f} },
    { { 0.030220f,  0.000000f,  0.000000f,  1.000000f}, { 0.994317f,  0.001896f, -0.000132f,  0.106446f} },
    { { 0.018187f,  0.000000f,  0.000000f,  1.000000f}, { 0.995931f, -0.002010f, -0.052079f, -0.073526f} },
    { { 0.018018f,  0.000000f, -0.000000f,  1.000000f}, { 1.000000f,  0.000000f,  0.000000f,  0.000000f} },
    { {-0.006059f,  0.056285f,  0.060064f,  1.000000f}, { 0.737238f,  0.202745f,  0.594267f,  0.249441f} },
    { {-0.040416f, -0.043018f,  0.019345f,  1.000000f}, {-0.290331f,  0.623527f, -0.663809f, -0.293734f} },
    { {-0.039354f, -0.075674f,  0.047048f,  1.000000f}, {-0.187047f,  0.678062f, -0.659285f, -0.265683f} },
    { {-0.038340f, -0.090987f,  0.082579f,  1.000000f}, {-0.183037f,  0.736793f, -0.634757f, -0.143936f} },
    { {-0.031806f, -0.087214f,  0.121015f,  1.000000f}, {-0.003659f,  0.758407f, -0.639342f, -0.126678f} },
    };

    //default open hand pose (right hand)
   vr::VRBoneTransform_t right_open_hand_pose[N_BONES] = {
   { { 0.000000f,  0.000000f,  0.000000f,  1.000000f}, { 1.000000f, -0.000000f, -0.000000f,  0.000000f} },
   { { 0.034038f,  0.036503f,  0.164722f,  1.000000f}, {-0.055147f, -0.078608f,  0.920279f, -0.379296f} },
   { { 0.012083f,  0.028070f,  0.025050f,  1.000000f}, { 0.567418f, -0.464112f,  0.623374f, -0.272106f} },
   { {-0.040406f, -0.000000f,  0.000000f,  1.000000f}, { 0.994838f,  0.082939f,  0.019454f,  0.055130f} },
   { {-0.032517f, -0.000000f, -0.000000f,  1.000000f}, { 0.974793f, -0.003213f,  0.021867f, -0.222015f} },
   { {-0.030464f,  0.000000f,  0.000000f,  1.000000f}, { 1.000000f, -0.000000f, -0.000000f,  0.000000f} },
   { {-0.000632f,  0.026866f,  0.015002f,  1.000000f}, { 0.421979f, -0.644251f,  0.422133f,  0.478202f} },
   { {-0.074204f,  0.005002f, -0.000234f,  1.000000f}, { 0.995332f,  0.007007f, -0.039124f,  0.087949f} },
   { {-0.043930f,  0.000000f,  0.000000f,  1.000000f}, { 0.997891f,  0.045808f,  0.002142f, -0.045943f} },
   { {-0.028695f, -0.000000f, -0.000000f,  1.000000f}, { 0.999649f,  0.001850f, -0.022782f, -0.013409f} },
   { {-0.022821f, -0.000000f,  0.000000f,  1.000000f}, { 1.000000f, -0.000000f,  0.000000f, -0.000000f} },
   { {-0.002177f,  0.007120f,  0.016319f,  1.000000f}, { 0.541276f, -0.546723f,  0.460749f,  0.442520f} },
   { {-0.070953f, -0.000779f, -0.000997f,  1.000000f}, { 0.980294f, -0.167261f, -0.078959f,  0.069368f} },
   { {-0.043108f, -0.000000f, -0.000000f,  1.000000f}, { 0.997947f,  0.018493f,  0.013192f,  0.059886f} },
   { {-0.033266f, -0.000000f, -0.000000f,  1.000000f}, { 0.997394f, -0.003328f, -0.028225f, -0.066315f} },
   { {-0.025892f,  0.000000f, -0.000000f,  1.000000f}, { 0.999195f, -0.000000f,  0.000000f,  0.040126f} },
   { {-0.000513f, -0.006545f,  0.016348f,  1.000000f}, { 0.550143f, -0.516692f,  0.429888f,  0.495548f} },
   { {-0.065876f, -0.001786f, -0.000693f,  1.000000f}, { 0.990420f, -0.058696f, -0.101820f,  0.072495f} },
   { {-0.040697f, -0.000000f, -0.000000f,  1.000000f}, { 0.999545f, -0.002240f,  0.000004f,  0.030081f} },
   { {-0.028747f,  0.000000f,  0.000000f,  1.000000f}, { 0.999102f, -0.000721f, -0.012693f,  0.040420f} },
   { {-0.022430f,  0.000000f, -0.000000f,  1.000000f}, { 1.000000f,  0.000000f,  0.000000f,  0.000000f} },
   { { 0.002478f, -0.018981f,  0.015214f,  1.000000f}, { 0.523940f, -0.526918f,  0.326740f,  0.584025f} },
   { {-0.062878f, -0.002844f, -0.000332f,  1.000000f}, { 0.986609f, -0.059615f, -0.135163f,  0.069132f} },
   { {-0.030220f, -0.000000f, -0.000000f,  1.000000f}, { 0.994317f,  0.001896f, -0.000132f,  0.106446f} },
   { {-0.018187f, -0.000000f, -0.000000f,  1.000000f}, { 0.995931f, -0.002010f, -0.052079f, -0.073526f} },
   { {-0.018018f, -0.000000f,  0.000000f,  1.000000f}, { 1.000000f,  0.000000f,  0.000000f,  0.000000f} },
   { { 0.006059f,  0.056285f,  0.060064f,  1.000000f}, { 0.737238f,  0.202745f, -0.594267f, -0.249441f} },
   { { 0.040416f, -0.043018f,  0.019345f,  1.000000f}, {-0.290331f,  0.623527f,  0.663809f,  0.293734f} },
   { { 0.039354f, -0.075674f,  0.047048f,  1.000000f}, {-0.187047f,  0.678062f,  0.659285f,  0.265683f} },
   { { 0.038340f, -0.090987f,  0.082579f,  1.000000f}, {-0.183037f,  0.736793f,  0.634757f,  0.143936f} },
   { { 0.031806f, -0.087214f,  0.121015f,  1.000000f}, {-0.003659f,  0.758407f,  0.639342f,  0.126678f} },
    };

};


/*
* Testing controller index for left glove
*/

TEST_F(ControllDriver, testControlIndex_Left)
{
    SGCore::SG::SenseGlove leftglove;
    SGCore::SG::SenseGlove rightglove;
    SenseGloveConnect<SGCore::SG::SenseGlove>* connect = SenseGloveConnect<SGCore::SG::SenseGlove>::getInstance(leftglove, rightglove);
    controllerDriver = new CSampleControllerDriver<SGCore::SG::SenseGlove>(true, connect);
    controllerDriver->SetControllerIndex(1);
    std::string controllerIndex = controllerDriver->GetSerialNumber();
    EXPECT_STREQ("LeftDK1", controllerIndex.c_str());
    connect->dispose();
    delete controllerDriver;
}

/*
* Testing controller index for right glove
*/


TEST_F(ControllDriver, testControlIndex_Right)
{
    SGCore::SG::SenseGlove leftglove;
    SGCore::SG::SenseGlove rightglove;
    SenseGloveConnect<SGCore::SG::SenseGlove>* connect = SenseGloveConnect<SGCore::SG::SenseGlove>::getInstance(leftglove, rightglove);
    controllerDriver = new CSampleControllerDriver<SGCore::SG::SenseGlove>(false, connect);
    controllerDriver->SetControllerIndex(2);
    std::string controllerIndex = controllerDriver->GetSerialNumber();
    EXPECT_STREQ("RightDK1", controllerIndex.c_str());
    connect->dispose();
    delete controllerDriver;
}




/*
* Testing get pose with left glove when left glove is not connected
*/

TEST_F(ControllDriver, testGetPose_Left)
{
    SGCore::SG::SenseGlove leftglove;
    SGCore::SG::SenseGlove rightglove;
    SenseGloveConnect<SGCore::SG::SenseGlove>* connect = SenseGloveConnect<SGCore::SG::SenseGlove>::getInstance(leftglove, rightglove);
    controllerDriver = new CSampleControllerDriver<SGCore::SG::SenseGlove>(true, connect);
    vr::DriverPose_t pose = controllerDriver->GetPose();
    vr::DriverPose_t zero = { 0 };
    
    EXPECT_EQ(sizeof(pose.vecPosition), sizeof(zero.vecPosition));

    for (int i = 0; i < 3; ++i) {
        EXPECT_EQ(pose.vecPosition[i], zero.vecPosition[i]) << "Vectors differ at index " << i;
    }
    connect->dispose();
    delete controllerDriver;
}

/*
* Testing get pose with right glove when right glove is not connected
*/

TEST_F(ControllDriver, testGetPose_Right)
{
    SGCore::SG::SenseGlove leftglove;
    SGCore::SG::SenseGlove rightglove;
    SenseGloveConnect<SGCore::SG::SenseGlove>* connect = SenseGloveConnect<SGCore::SG::SenseGlove>::getInstance(leftglove, rightglove);
    controllerDriver = new CSampleControllerDriver<SGCore::SG::SenseGlove>(false, connect);
    vr::DriverPose_t pose = controllerDriver->GetPose();
    vr::DriverPose_t zero = { 0 };

    EXPECT_EQ(sizeof(pose.vecPosition), sizeof(zero.vecPosition));

    for (int i = 0; i < 3; ++i) {
        EXPECT_EQ(pose.vecPosition[i], zero.vecPosition[i]) << "Vectors differ at index " << i;
    }
    connect->dispose();
    delete controllerDriver;
}

/*
* Testing the position updates in GetPose for a left glove
*/

#if defined(_WINDOWS)
TEST_F(ControllDriver, mockTestKeyPress_Left)
{
    MockSenseGlove leftglove;
    MockSenseGlove rightglove;
    SenseGloveConnect<MockSenseGlove>* connect = SenseGloveConnect<MockSenseGlove>::getInstance(leftglove, rightglove);
    mockControllerDriver = new CSampleControllerDriver<MockSenseGlove>(false, connect);
    mockControllerDriver->SetControllerIndex(1);

    INPUT input
    input = { 87 };
    input.type = INPUT_KEYBOARD;
    input.ki.wVk = 'W';
    input.ki.wScan = 'W';
    input.ki.dwFlags = 0; // key down
    SendInput(1, &input, sizeof(INPUT));
    assert(GetAsyncKeyState('W') < 0);

    vr::DriverPose_t pose = mockControllerDriver->GetPose();


    
    EXPECT_EQ(pose.vecPosition[2],  -0.01);
    


    connect->dispose();
    delete mockControllerDriver;
}
#endif
/*
* Testing the bones array for the left hand in RunFrame
*/

TEST_F(ControllDriver, mockTestRunFrame_LeftBones)
{
    MockSenseGlove leftglove;
    MockSenseGlove rightglove;
    SenseGloveConnect<MockSenseGlove>* connect = SenseGloveConnect<MockSenseGlove>::getInstance(leftglove, rightglove);
    mockControllerDriver = new CSampleControllerDriver<MockSenseGlove>(false, connect);
    mockControllerDriver->SetControllerIndex(1);

    mockControllerDriver->RunFrame();
    
    for(int i=0; i<N_BONES; i++){
        EXPECT_EQ(mockControllerDriver->bones[i], left_open_hand_pose[i]);
	}

    connect->dispose();
    delete mockControllerDriver;
}


/*
* Testing the bones array for the right hand in RunFrame
*/

TEST_F(ControllDriver, mockTestRunFrame_RightBones)
{
    MockSenseGlove leftglove;
    MockSenseGlove rightglove;
    SenseGloveConnect<MockSenseGlove>* connect = SenseGloveConnect<MockSenseGlove>::getInstance(leftglove, rightglove);
    mockControllerDriver = new CSampleControllerDriver<MockSenseGlove>(false, connect);
    mockControllerDriver->SetControllerIndex(2);

    mockControllerDriver->RunFrame();

    for (int i = 0; i < N_BONES; i++) {
        EXPECT_EQ(mockControllerDriver->bones[i], right_open_hand_pose[i]);
    }

    connect->dispose();
    delete mockControllerDriver;
}
