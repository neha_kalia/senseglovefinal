#include <string>
#include <gtest/gtest.h>
#include "gmock/gmock.h"
#include "../samples/driver_sample/SenseGloveConnect.cpp"
#include "MockSenseGlove.h"


/**
* Tests for SenseGloveConnect class
*/
     

/**
* Defining an == method for SenseGlove, GlovePose and SensorData
*/
namespace SGCore {
    namespace SG
    {
        bool operator==(SenseGlove p1, SenseGlove p2) {
            return p1.GetGloveModel().Equals(p2.GetGloveModel()); 
        }
    }
}

namespace SGCore {
    namespace SG
    {
        bool operator==(SG_SensorData p1, SG_SensorData p2) {
            return p1.Equals(p2);
        }
    }
}



namespace SGCore {
    namespace SG
    {
        bool operator==(SG_GlovePose p1, SG_GlovePose p2) {
            return p1.Equals(p2);
        }
    }
}

/**
* Adding a MockSenseGlove Class for testing purposes. 
* This class is used when a method calls a SenseGlove method
* and there requires to be an active connection.
*/

/*
class MockSenseGlove  {
public:
  */
    /**
    * These methods are here in place of mock methods. 
    * They are used to test when a glove is connected.
    * @return true since the glove is connected, thus the glove information was found. 
    */
 /*
    bool GetGlovePose(SGCore::SG::SG_GlovePose& glovePose){
        return true;

	}

    bool GetSensorData(SGCore::SG::SG_SensorData& sData) {
        return true;
	}

    static bool GetSenseGlove(bool rightHanded, MockSenseGlove& glove) {
        return true;
	}

    bool GetGlovePose(SGCore::SG::SG_SensorData& sData, SGCore::SG::SG_GlovePose& glovePose){
        return true;
	}

    bool SendHaptics(const SGCore::Haptics::SG_BuzzCmd& buzzCmd){
        return true;
	}


};
*/
//Setting up the test fixture
struct SenseGloveConnectTest : public ::testing::Test
{

    SGCore::SG::SenseGlove leftglove;
    SGCore::SG::SenseGlove rightglove;
    MockSenseGlove mleftglove;
    MockSenseGlove mrightglove;

};


/**
* Test for getGlovePose for the right glove when no glove is connected.
* Deserialized right glove should be found.
*/
TEST_F(SenseGloveConnectTest, testing_RightDeserialized) {
    SenseGloveConnect<SGCore::SG::SenseGlove>* connect = SenseGloveConnect<SGCore::SG::SenseGlove>::getInstance(leftglove, rightglove);
    bool des = connect->rightGloveDeserialized;
    bool gloveFound = connect->rightGloveFound;
    EXPECT_EQ(gloveFound, false);
    EXPECT_EQ(des, true);
    connect->dispose();

};

/**
* Tests Singleton with mocked glove. Only one instance is can be used.
*/

TEST_F(SenseGloveConnectTest, testing_instance_mocked_glove) {
    SenseGloveConnect<MockSenseGlove>* connect =  SenseGloveConnect<MockSenseGlove>::getInstance(mleftglove, mrightglove);
    MockSenseGlove glove2;
    MockSenseGlove glove3;
    SenseGloveConnect<MockSenseGlove>* connect2 = SenseGloveConnect<MockSenseGlove>::getInstance(glove2, glove3);
    EXPECT_EQ(connect, connect2);
    connect->dispose();
    connect2->dispose();
};

/**
* Tests Singleton with sense glove. Only one instance is can be used.
*/

TEST_F(SenseGloveConnectTest, testing_instance_senseglove) {
    SenseGloveConnect <SGCore::SG::SenseGlove> * connect = SenseGloveConnect<SGCore::SG::SenseGlove>::getInstance(leftglove, rightglove);
    SGCore::SG::SenseGlove glove2;
    SGCore::SG::SenseGlove glove3;
    SenseGloveConnect<SGCore::SG::SenseGlove>* connect2 = SenseGloveConnect<SGCore::SG::SenseGlove>::getInstance(glove2, glove3);
    EXPECT_EQ(connect, connect2);
    connect->dispose();
    connect2->dispose();
};

/**
* Test for getGlovePose for the left glove when no glove is connected.
*/

TEST_F(SenseGloveConnectTest, testing_glovePoseNoGlove_Left) {
    SenseGloveConnect<SGCore::SG::SenseGlove>* connect = SenseGloveConnect<SGCore::SG::SenseGlove>::getInstance(leftglove, rightglove);
    SGCore::SG::SG_GlovePose glovePose;
    bool ans = connect->getGlovePose(glovePose, true);
    EXPECT_EQ(ans, false);
    connect->dispose();
 };

/** 
* Test for getGlovePose for the right glove when no glove is connected. 
*/

TEST_F(SenseGloveConnectTest, testing_glovePoseNoGlove_Right) {
    SenseGloveConnect<SGCore::SG::SenseGlove>* connect = SenseGloveConnect<SGCore::SG::SenseGlove>::getInstance(leftglove, rightglove);
    SGCore::SG::SG_GlovePose glovePose;
    connect->rightGloveDeserialized = false;
    bool ans = connect->getGlovePose(glovePose, false);
    EXPECT_EQ(ans, false);
    connect->dispose();

};


/**
* Test for getGlove when the left glove is not connected.
*/

TEST_F(SenseGloveConnectTest, testing_NoGlove_Left) {
    SenseGloveConnect<SGCore::SG::SenseGlove>* connect = SenseGloveConnect<SGCore::SG::SenseGlove>::getInstance(leftglove, rightglove);
    SGCore::SG::SenseGlove senseglove;
    SGCore::SG::SenseGlove::GetSenseGlove(senseglove);
    bool ans= connect->getGlove(true, senseglove);
    EXPECT_EQ(ans, false);
    connect->dispose();

};

/**
* Test for getGlove when the right glove is not connected.
*/

TEST_F(SenseGloveConnectTest, testing_NoGlove_Right) {
    SenseGloveConnect<SGCore::SG::SenseGlove>* connect = SenseGloveConnect<SGCore::SG::SenseGlove>::getInstance(leftglove, rightglove);
    SGCore::SG::SenseGlove senseglove;
    bool ans = connect->getGlove(false, senseglove);
    EXPECT_EQ(ans, false);
    connect->dispose();

};

/**
* Test for getGlove when the left glove is found.
*/

TEST_F(SenseGloveConnectTest, testing_getGloveLeft) {
    SenseGloveConnect<MockSenseGlove>* connect = SenseGloveConnect<MockSenseGlove>::getInstance(mleftglove, mrightglove);
    connect->leftGloveFound = true;
    bool ans = connect->getGlove(true, mleftglove);
    
    EXPECT_EQ(ans, true);
    connect->dispose();
};

/**
* Test for getGlove when the right glove is found.
*/

TEST_F(SenseGloveConnectTest, testing_getGloveRight) {
    SenseGloveConnect<MockSenseGlove>* connect = SenseGloveConnect<MockSenseGlove>::getInstance(mleftglove, mrightglove);
    connect->rightGloveFound = true;
    bool ans = connect->getGlove(false, mrightglove);
    EXPECT_EQ(ans, true);
    connect->dispose();
};

/*
* Testing get glove pose with a left glove.
*/

TEST_F(SenseGloveConnectTest, testing_getGlovePoseLeft) {
    SGCore::SG::SG_GlovePose glovePose;
    SenseGloveConnect<MockSenseGlove>* connect = SenseGloveConnect<MockSenseGlove>::getInstance(mleftglove, mrightglove);
    bool ans = connect->getGlovePose(glovePose, true);
    EXPECT_TRUE(ans);
    connect->dispose();
}

/*
* Testing get glove pose with a right glove.
*/

TEST_F(SenseGloveConnectTest, testing_getGlovePoseRight) {
    SGCore::SG::SG_GlovePose glovePose;
    SenseGloveConnect<MockSenseGlove>* connect = SenseGloveConnect<MockSenseGlove>::getInstance(mleftglove, mrightglove);
    bool ans = connect->getGlovePose(glovePose, false);
    EXPECT_TRUE(ans);
    connect->dispose();
}

/*
* Testing get hand profile with for a right hand
*/

TEST_F(SenseGloveConnectTest, testing_geHandProfile_Right) {
    SGCore::SG::SG_HandProfile handProfile;
    SenseGloveConnect<MockSenseGlove>* connect = SenseGloveConnect<MockSenseGlove>::getInstance(mleftglove, mrightglove);
    bool ans = connect->getHandProfile(false, handProfile);
    EXPECT_TRUE(ans);
    connect->dispose();

}

/*
* Testing get hand profile with for a left hand
*/

TEST_F(SenseGloveConnectTest, testing_geHandProfile_Left) {
    SGCore::SG::SG_HandProfile handProfile;
    SenseGloveConnect<MockSenseGlove>* connect = SenseGloveConnect<MockSenseGlove>::getInstance(mleftglove, mrightglove);
    bool ans = connect->getHandProfile(true, handProfile);
    EXPECT_TRUE(ans);
    connect->dispose();

}


