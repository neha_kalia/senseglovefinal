#include "../headers/csamplecontrollerdriver.h"

using namespace vr;

static double cyaw = 0, cpitch = 0, croll = 0;
static double cpX = 0, cpY = 0, cpZ = 0;
static double ct0, ct1, ct2, ct3, ct4, ct5;

static double c2pX = 0, c2pY = 0, c2pZ = 0;

/*
* Templatizing the csamplecontrollerdriver class for testing purposes.
* All methods are implemented using the typename T.
* This is to allow the methods to work with both the SenseGlove and MockSenseGlove.
* MockSenseGlove is only used by the tests. For all other instances, SenseGlove is used.
*/


template <typename T>
CSampleControllerDriver<T>::CSampleControllerDriver(bool isLeftGlove, SenseGloveConnect<T>* Connect)
{
    m_unObjectId = vr::k_unTrackedDeviceIndexInvalid;
    m_ulPropertyContainer = vr::k_ulInvalidPropertyContainer;
    leftGlove = isLeftGlove;
    connect = Connect;
    connect->getGlove(isLeftGlove, glove);

}

template CSampleControllerDriver<SGCore::SG::SenseGlove>::CSampleControllerDriver(bool isLeftGlove, SenseGloveConnect<SGCore::SG::SenseGlove>* Connect);


/**
 * Sets the controller index to the desired integer - 1 for the left glove, 2 for the right glove.
 * 
 * @param CtrlIndex Index used to identify the glove
 */
template <typename T>
void CSampleControllerDriver<T>::SetControllerIndex(int32_t CtrlIndex)
{
    ControllerIndex = CtrlIndex;
}

template void CSampleControllerDriver<SGCore::SG::SenseGlove>::SetControllerIndex(int32_t CtrlIndex);
/**
 * Deletes the pointer for SenseGloveConnect.
 */
template <typename T>
CSampleControllerDriver<T>::~CSampleControllerDriver()
{
    connect->dispose();
}

template CSampleControllerDriver<SGCore::SG::SenseGlove>::~CSampleControllerDriver();

/**
 * Activates the driver and creates the scalar, boolean and skeletal components as required.
 *
 */
template <typename T>
vr::EVRInitError CSampleControllerDriver<T>::Activate(vr::TrackedDeviceIndex_t unObjectId)
{
    m_unObjectId = unObjectId;
    m_ulPropertyContainer = vr::VRProperties()->TrackedDeviceToPropertyContainer(m_unObjectId);
    vr::VRProperties()->SetStringProperty(m_ulPropertyContainer, vr::Prop_ControllerType_String, "vive_controller");
    vr::VRProperties()->SetStringProperty(m_ulPropertyContainer, vr::Prop_LegacyInputProfile_String, "vive_controller");


    vr::VRProperties()->SetStringProperty(m_ulPropertyContainer, vr::Prop_ModelNumber_String, "ViveMV");
    vr::VRProperties()->SetStringProperty(m_ulPropertyContainer, vr::Prop_ManufacturerName_String, "HTC");
    vr::VRProperties()->SetStringProperty(m_ulPropertyContainer, vr::Prop_RenderModelName_String, "vr_controller_vive_1_5");

    vr::VRProperties()->SetStringProperty(m_ulPropertyContainer, Prop_TrackingSystemName_String, "VR Controller");
    vr::VRProperties()->SetInt32Property(m_ulPropertyContainer, Prop_DeviceClass_Int32, TrackedDeviceClass_Controller);

    //set serial number based on the glove you are using
    switch (ControllerIndex) {
    case 1:
        vr::VRProperties()->SetStringProperty(m_ulPropertyContainer, Prop_SerialNumber_String, "LeftDK1");
        break;
    case 2:
        vr::VRProperties()->SetStringProperty(m_ulPropertyContainer, Prop_SerialNumber_String, "RightDK1");
        break;
    }

    uint64_t supportedButtons = 0xFFFFFFFFFFFFFFFFULL;
    vr::VRProperties()->SetUint64Property(m_ulPropertyContainer, vr::Prop_SupportedButtons_Uint64, supportedButtons);

    switch (ControllerIndex) {
    case 1:
        vr::VRProperties()->SetInt32Property(m_ulPropertyContainer, Prop_ControllerRoleHint_Int32, TrackedControllerRole_LeftHand);
        break;
    case 2:
        vr::VRProperties()->SetInt32Property(m_ulPropertyContainer, Prop_ControllerRoleHint_Int32, TrackedControllerRole_RightHand);
        break;
    }

    // this file tells the UI what to show the user for binding this controller as well as what default bindings should
    // be for legacy or other apps
    vr::VRProperties()->SetStringProperty(m_ulPropertyContainer, Prop_InputProfilePath_String, "{null}/input/mycontroller_profile.json");

    //  Buttons handles
    vr::VRDriverInput()->CreateBooleanComponent(m_ulPropertyContainer, "/input/application_menu/click", &HButtons[0]);
    vr::VRDriverInput()->CreateBooleanComponent(m_ulPropertyContainer, "/input/grip/click", &HButtons[1]);
    vr::VRDriverInput()->CreateBooleanComponent(m_ulPropertyContainer, "/input/system/click", &HButtons[2]);
    vr::VRDriverInput()->CreateBooleanComponent(m_ulPropertyContainer, "/input/trackpad/click", &HButtons[3]);

    // Analog handles
    vr::VRDriverInput()->CreateScalarComponent(
        m_ulPropertyContainer, "/input/trackpad/x", &HAnalog[0],
        vr::EVRScalarType::VRScalarType_Absolute, vr::EVRScalarUnits::VRScalarUnits_NormalizedTwoSided
    );
    vr::VRDriverInput()->CreateScalarComponent(
        m_ulPropertyContainer, "/input/trackpad/y", &HAnalog[1],
        vr::EVRScalarType::VRScalarType_Absolute, vr::EVRScalarUnits::VRScalarUnits_NormalizedTwoSided
    );
    vr::VRDriverInput()->CreateScalarComponent(
        m_ulPropertyContainer, "/input/trigger/value", &HAnalog[2],
        vr::EVRScalarType::VRScalarType_Absolute, vr::EVRScalarUnits::VRScalarUnits_NormalizedOneSided
    );
    vr::VRProperties()->SetInt32Property(m_ulPropertyContainer, vr::Prop_Axis0Type_Int32, vr::k_eControllerAxis_TrackPad);

    // create our haptic component
    vr::VRDriverInput()->CreateHapticComponent(m_ulPropertyContainer, "/output/haptic", &m_compHaptic);

    const char* pBasePosePath = "/pose/raw";
    const char* pchName;
    const char* pchSkeletonPath;
    //
    if (leftGlove) {
        pchName = "/input/skeleton/left";
        pchSkeletonPath = "/skeleton/hand/left";
    }
    else {
        pchName = "/input/skeleton/right";
        pchSkeletonPath = "/skeleton/hand/right";
    }

    EVRInputError input_error = vr::VRDriverInput()->CreateSkeletonComponent(m_ulPropertyContainer, pchName, pchSkeletonPath, pBasePosePath, nullptr, 0, &ulSkeletalComponentHandle);
    return VRInitError_None;
}

template vr::EVRInitError CSampleControllerDriver<SGCore::SG::SenseGlove>::Activate(vr::TrackedDeviceIndex_t unObjectId);

/**
 * Deactivates the driver by setting the object id to an invalid value.
 */
template <typename T>
void CSampleControllerDriver<T>::Deactivate()
{
    m_unObjectId = vr::k_unTrackedDeviceIndexInvalid;
}

template void CSampleControllerDriver<SGCore::SG::SenseGlove>::Deactivate();

/**
 * Add code for stand by mode if necessary.
 */
template <typename T>
void CSampleControllerDriver<T>::EnterStandby()
{
}

template void CSampleControllerDriver<SGCore::SG::SenseGlove>::EnterStandby();

template <typename T>
void* CSampleControllerDriver<T>::GetComponent(const char* pchComponentNameAndVersion)
{
    // override this to add a component to a driver
    return NULL;
}

template void* CSampleControllerDriver<SGCore::SG::SenseGlove>::GetComponent(const char* pchComponentNameAndVersion);

/**
 * Add code to be executed while powering off if necessary.
 */
template <typename T>
void CSampleControllerDriver<T>::PowerOff()
{
}

template void CSampleControllerDriver<SGCore::SG::SenseGlove>::PowerOff();

/**
 * Calculate positional and rotational data for the controller.
 * Using the keyboard you can move the controller.
 * Harcoded behaviour which you can toggle using the keyboard is defined here.
 * @return The positional data for the controller
 */
template <typename T>
void CSampleControllerDriver<T>::DebugRequest(const char* pchRequest, char* pchResponseBuffer, uint32_t unResponseBufferSize)
{
    if (unResponseBufferSize >= 1) {
        pchResponseBuffer[0] = 0;
    }
}
template void CSampleControllerDriver<SGCore::SG::SenseGlove>::DebugRequest(const char* pchRequest, char* pchResponseBuffer, uint32_t unResponseBufferSize);


/**
* Used to move the controller in world space.
* Hardcoded feedback behaviour is also defined here, which you can turn on/off or toggle using the keyboard.
* @return The position in world space.
*/

template <typename T>
DriverPose_t CSampleControllerDriver<T>::GetPose()
{
    DriverPose_t pose = { 0 };
    pose.poseIsValid = true;
    pose.result = TrackingResult_Running_OK;
    pose.deviceIsConnected = true;
    pose.qWorldFromDriverRotation = HmdQuaternion_Init(1, 0, 0, 0);
    pose.qDriverFromHeadRotation = HmdQuaternion_Init(1, 0, 0, 0);

    //show ability to use force feedback
    //this is temporary until OpenVR adds support fo force feedback
    if ((GetAsyncKeyState(VK_ADD) & 0x8000) != 0) {
        //toggle on force feedback by pressing + on keyboard
        glove.SendHaptics(SGCore::Haptics::SG_FFBCmd(80, 80, 80, 80, 80));
    }
    if ((GetAsyncKeyState(VK_SUBTRACT) & 0x8000) != 0) {
        //toggle off force feedback by pressing - on keyboard
        glove.SendHaptics(SGCore::Haptics::SG_FFBCmd::off);
    }

    // vibrating by key. This code is an extension;
    // In ProcessEvent this event is already mapped, which is the important part
    if ((GetAsyncKeyState(VK_MULTIPLY) & 0x8000) != 0) {
        //toggle on vibrating by pressing * on keyboard
        glove.SendHaptics(SGCore::Haptics::SG_BuzzCmd(80, 80, 80, 80, 80));
    }
    if ((GetAsyncKeyState(VK_DIVIDE) & 0x8000) != 0) {
        //toggle off vibrating by pressing / on keyboard
        glove.SendHaptics(SGCore::Haptics::SG_BuzzCmd::off);
    }

    //to toggle the wrist rotations by the IMU. Press Left Shift and Right Shift at the same time
    if ((GetAsyncKeyState(VK_LSHIFT) & GetAsyncKeyState(VK_RSHIFT) & 0x8000) != 0) {
        if (IMU_enabled == false) {
            IMU_enabled = true;
        }
        else {
            IMU_enabled = false;
        }
    }

    //moving the glove(s) using the keyboard
    if (ControllerIndex == 1) {
        if ((GetAsyncKeyState(72) & 0x8000) != 0) {
            cyaw += -0.1;                                       //H
        }
        if ((GetAsyncKeyState(84) & 0x8000) != 0) {
            croll += 0.1;                                       //T
        }
        if ((GetAsyncKeyState(71) & 0x8000) != 0) {
            croll += -0.1;                                       //G
        }
        if ((GetAsyncKeyState(66) & 0x8000) != 0) { //B
            cpitch = 0;
            croll = 0;
        }

        //Change position controller1
        if ((GetAsyncKeyState(87) & 0x8000) != 0) {
            cpZ += -0.01;                                       //W
        }
        if ((GetAsyncKeyState(83) & 0x8000) != 0) {
            cpZ += 0.01;                                       //S
        }
        if ((GetAsyncKeyState(65) & 0x8000) != 0) {
            cpX += -0.01;                                       //A
        }
        if ((GetAsyncKeyState(68) & 0x8000) != 0) {
            cpX += 0.01;                                       //D
        }
        if ((GetAsyncKeyState(81) & 0x8000) != 0) {
            cpY += 0.01;                                       //Q
        }
        if ((GetAsyncKeyState(69) & 0x8000) != 0) {
            cpY += -0.01;                                       //E
        }
        if ((GetAsyncKeyState(82) & 0x8000) != 0) {
            cpX = 0;
            cpY = 0;
            cpZ = 0;
        }                                                                        //R

        pose.vecPosition[0] = cpX;
        pose.vecPosition[1] = cpY;
        pose.vecPosition[2] = cpZ;
    }
    else {
        //Controller2

        if ((GetAsyncKeyState(70) & 0x8000) != 0) {
            cyaw += 0.1;                                       //F
        }
        if ((GetAsyncKeyState(72) & 0x8000) != 0) {
            cyaw += -0.1;                                       //H
        }
        if ((GetAsyncKeyState(84) & 0x8000) != 0) {
            croll += 0.1;                                       //T
        }
        if ((GetAsyncKeyState(71) & 0x8000) != 0) {
            croll += -0.1;                                       //G
        }
        if ((GetAsyncKeyState(73) & 0x8000) != 0) {
            c2pZ += -0.01;                                       //I
        }
        if ((GetAsyncKeyState(75) & 0x8000) != 0) {
            c2pZ += 0.01;                                       //K
        }
        if ((GetAsyncKeyState(74) & 0x8000) != 0) {
            c2pX += -0.01;                                       //J
        }
        if ((GetAsyncKeyState(76) & 0x8000) != 0) {
            c2pX += 0.01;                                       //L
        }
        if ((GetAsyncKeyState(85) & 0x8000) != 0) {
            c2pY += 0.01;                                       //U
        }
        if ((GetAsyncKeyState(79) & 0x8000) != 0) {
            c2pY += -0.01;                                       //O
        }
        if ((GetAsyncKeyState(80) & 0x8000) != 0) {
            c2pX = 0;
            c2pY = 0;
            c2pZ = 0;
        }                                                                           //P

        pose.vecPosition[0] = c2pX;
        pose.vecPosition[1] = c2pY;
        pose.vecPosition[2] = c2pZ;
    }

    //return updated rotation and location of pose
    return pose;


}

template DriverPose_t CSampleControllerDriver<SGCore::SG::SenseGlove>::GetPose();

/**
* Maps input from senseglove to openvr format and updates the skeletal pose.
* takes the open hand pose for each hand as a base and updates the orientation of each of the finger joints to capture the movement.
*/
template <typename T>
void CSampleControllerDriver<T>::RunFrame()
{
#if defined(_WINDOWS)

    if (ControllerIndex == 1 || ControllerIndex == 2) {

        if (ControllerIndex == 1) {
            for (int i = 0; i < N_BONES; i++) {
                bones[i] = left_open_hand_pose[i];
            }
        }

        else {
            for (int i = 0; i < N_BONES; i++) {
                bones[i] = right_open_hand_pose[i];
            }
        }

        SGCore::SG::SG_GlovePose glovePose;
        connect->getGlovePose(glovePose, leftGlove);
        SGCore::SG::SG_SensorData glovedata;


        if (!glovePose.jointPositions.empty()) {
            SGCore::SG::SG_HandProfile profile;
            connect->getHandProfile(leftGlove, profile);
            SGCore::HandPose hand = T::CalculateHandPose(glovePose, profile, SGCore::SG::SG_Solver::Interpolation);

            glove.GetSensorData(glovedata);
            SGCore::Kinematics::Quat imuValues = glovedata.imuValues;

            uint16_t c = 3;

            for (size_t i = 0; i < hand.jointPositions.size(); i++) {
                //i represents each finger

                size_t j = 0;
                if (i == 0) {
                    j = 1;
                }

                for (; j < hand.jointPositions[i].size(); j++) {
                    //j represents each joint

                    if (j == 0 && i != 0) {
                        //skip over meta bones, they're already initialized in open_hand_pose
                        c++;
                    }

                    SGCore::Kinematics::Vect3D currentAngle;
                    if (j == hand.handAngles[i].size()) {
                        //handangles doesn't have an angle for the fingertip, so set it to rotation of previous joint
                        //not used for skinning, so the values here don't matter
                        currentAngle = hand.handAngles[i][j - 1];
                    }
                    else {
                        currentAngle = hand.handAngles[i][j];
                    }


                    SGCore::Kinematics::Quat quaternion = SGCore::Kinematics::Quat::FromEuler(currentAngle.x, currentAngle.y, currentAngle.z);
                    //left hand has inverted finger direction : -z
                    if (ControllerIndex == 1) {
                        bones[c].orientation = { quaternion.w, -quaternion.x , -quaternion.z, quaternion.y };
                    }
                    //right hand has normal case.
                    else {
                        bones[c].orientation = { quaternion.w, -quaternion.x , quaternion.z, quaternion.y };
                    }
                    c++;
                }
            }


            // used for the angle of the wrist calculated by the IMU
            if (IMU_enabled == true) {
                bones[1].orientation = { bones[1].orientation.w, -imuValues.z,  imuValues.x, imuValues.y };
            }

            //auxillary bones
            bones[26] = bones[4];
            bones[27] = bones[9];
            bones[28] = bones[14];
            bones[29] = bones[19];
            bones[30] = bones[24];



            VRBoneTransform_t* bonesTrans = bones;
            vr::EVRInputError err = vr::VRDriverInput()->UpdateSkeletonComponent(ulSkeletalComponentHandle, vr::VRSkeletalMotionRange_WithController, bonesTrans, N_BONES);
            EVRInputError update_error = vr::VRDriverInput()->UpdateSkeletonComponent(ulSkeletalComponentHandle, vr::VRSkeletalMotionRange_WithoutController, bonesTrans, N_BONES);
            if (update_error != EVRInputError::VRInputError_None) {
                system("pause");
            }
        }

        if (m_unObjectId != vr::k_unTrackedDeviceIndexInvalid) {
            vr::VRServerDriverHost()->TrackedDevicePoseUpdated(m_unObjectId, GetPose(), sizeof(DriverPose_t));
        }

    }


#endif
}
template void CSampleControllerDriver<SGCore::SG::SenseGlove>::RunFrame();


/**
 * Process the virtual reality event data to send feedback to the controller.
 * 
 * @param vrEvent The event to process
 */
template <typename T>
void CSampleControllerDriver<T>::ProcessEvent(const vr::VREvent_t& vrEvent)
{
    T glove;
    switch (vrEvent.eventType) {
    case vr::VREvent_Input_HapticVibration:
        if (vrEvent.data.hapticVibration.componentHandle == m_compHaptic) {
            if (ControllerIndex == 1 && connect->getGlove(true, glove)) {
                glove.SendHaptics(SGCore::Haptics::SG_BuzzCmd(80, 80, 80, 80, 80));
            }
            else if (connect->getGlove(false, glove)) {
                glove.SendHaptics(SGCore::Haptics::SG_BuzzCmd(80, 80, 80, 80, 80));
            }
        }
        break;
    }
}

template void CSampleControllerDriver<SGCore::SG::SenseGlove>::ProcessEvent(const vr::VREvent_t& vrEvent);
/**
 * Get the serial number for this controller.
 * 
 * @return The serial number depending on whether the controller is a right glove or a left glove
 */
template <typename T>
std::string CSampleControllerDriver<T>::GetSerialNumber() const
{
    switch (ControllerIndex) {
    case 1:
        return "LeftDK1";
        break;
    case 2:
        return "RightDK1";
        break;
    }
    return nullptr;
}

template std::string CSampleControllerDriver<SGCore::SG::SenseGlove>::GetSerialNumber() const;
