#include "../headers/cserverdriver_sample.h"
#include "../headers/SenseGloveConnect.h"

using namespace vr;

/**
 * Initializes the drivers according to the hardware present.
 * 
 * Creates a dummy device driver to simulate an hmd.
 * Creates controller drivers for the connected gloves.
 * 
 * A maximum of two controller drivers can be created, one for the left glove and one for the right.
 * If more gloves are connected, the driver will only read the first connected glove for each side.
 * 
 * @return VRInitError_None if everything can be initialized without causing crashes
 */
EVRInitError CServerDriver_Sample::Init(vr::IVRDriverContext *pDriverContext)
{
    VR_INIT_SERVER_DRIVER_CONTEXT(pDriverContext);


    //InitDriverLog( vr::VRDriverLog() );
    /**SenseGloveConnect* connect = SenseGloveConnect::getInstance();
    SGCore::SG::SenseGlove& myGlove = connect->getGlove();

    myGlove.SendHaptics(SGCore::Haptics::SG_BuzzCmd(80, 80, 80, 80, 80)); //vibrate the index fingerat 80% intensity.
    std::this_thread::sleep_for(std::chrono::milliseconds(200)); //vibrating for for 200ms.

    myGlove.SendHaptics(SGCore::Haptics::SG_BuzzCmd::off); //turn off all Buzz Motors.
    std::this_thread::sleep_for(std::chrono::milliseconds(10)); //wait for 10ms.

    connect->dispose();**/

    SGCore::SG::SenseGlove Leftglove;
    SGCore::SG::SenseGlove Rightglove;
    connect = SenseGloveConnect<SGCore::SG::SenseGlove>::getInstance(Leftglove, Rightglove);

    m_pNullHmdLatest = new CSampleDeviceDriver();
    vr::VRServerDriverHost()->TrackedDeviceAdded(m_pNullHmdLatest->GetSerialNumber().c_str(), vr::TrackedDeviceClass_HMD, m_pNullHmdLatest);

   
    if (connect->leftGloveFound) {
        m_pController = new CSampleControllerDriver<SGCore::SG::SenseGlove>(true, connect);
        m_pController->SetControllerIndex(1);
        vr::VRServerDriverHost()->TrackedDeviceAdded(m_pController->GetSerialNumber().c_str(), vr::TrackedDeviceClass_Controller, m_pController);
    }

    if (connect->rightGloveFound || connect->rightGloveDeserialized) {
        m_pController2 = new CSampleControllerDriver<SGCore::SG::SenseGlove>(false, connect);
        m_pController2->SetControllerIndex(2);
        vr::VRServerDriverHost()->TrackedDeviceAdded(m_pController2->GetSerialNumber().c_str(), vr::TrackedDeviceClass_Controller, m_pController2);
    }

    return VRInitError_None;
}

/**
 * Deletes the pointers for the driver and SenseGloveConnect.
 */
void CServerDriver_Sample::Cleanup()
{
    //CleanupDriverLog();
    delete m_pNullHmdLatest;
    m_pNullHmdLatest = NULL;
    delete m_pController;
    m_pController = NULL;
    delete m_pController2;
    m_pController2 = NULL;
    connect->dispose();
    delete connect;
}

/**
 * Calls driver methods to update the state.
 */
void CServerDriver_Sample::RunFrame()
{
    if (m_pNullHmdLatest) {
       m_pNullHmdLatest->RunFrame();
    }
    if (m_pController) {
        m_pController->RunFrame();
    }
    if (m_pController2) {
        m_pController2->RunFrame();
    }

#if 0
    vr::VREvent_t vrEvent;
    while ( vr::VRServerDriverHost()->PollNextEvent( &vrEvent, sizeof( vrEvent ) ) )
    {
        if ( m_pController )
        {
            m_pController->ProcessEvent(vrEvent);
        }
        if ( m_pController2)
        {
            m_pController2->ProcessEvent(vrEvent);
        }
    }
#endif
}
