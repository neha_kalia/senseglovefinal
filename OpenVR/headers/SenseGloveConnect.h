// ----------------------------------------------------------------------------------
// This class establishes connected with the SenseGlove devices.
// Used to manage communication with the SenseGlove API
// ----------------------------------------------------------------------------------

#ifndef SENSEGLOVECONNECT_H
#define SENSEGLOVECONNECT_H

#include <iostream>
#include "SGConnect.h"
#include "DeviceList.h"
#include "SenseGlove.h"
#include "Serialize.h"

#include <thread>
#include <fstream>
#include <chrono> //wait untill we find SenseGloves

/*
* Templatizing the SenseGloveConnect Class for testing purposes.
*/

template <typename T>
//implements singleton design pattern
class SenseGloveConnect
{

private:
	static SenseGloveConnect* instance;
	T leftGlove;
	T rightGlove;
	SenseGloveConnect<T>(T& testgloveLeft, T& testgloveRight);

public:
	bool leftGloveFound;
	bool rightGloveFound;
	bool rightGloveDeserialized;
	
	/// <summary> Get the instance of the SenseGloveConnect singleton </summary>
	static SenseGloveConnect<T>* getInstance(T& left_glove, T& right_glove);

	/// <summary> Get the first connected left/ right glove </summary>
	bool getGlove(bool isLeftGlove, T& glove);

	/// <summary> Safely dispose the SGConnect object </summary>
	void dispose();

	/// <summary> Get the updated glove pose for the requested hand (right or left) </summary>
	bool getGlovePose(SGCore::SG::SG_GlovePose& glovePose, bool isLeftGlove);

	/// <summary> Get the default hand profile for the requested hand (right or left) </summary>
	bool getHandProfile(bool isLeftGlove, SGCore::SG::SG_HandProfile& handProfile);
};


#endif 

