#include "databaseConnect.h"
template<typename T>
class MyDatabase {
    T& dBC;
public:
    MyDatabase<T>(T& _dbc) : dBC(_dbc) {}

    int Init(std::string username, std::string password) {
        if (dBC.login(username, password) != true) {
            std::cout << "DB Failure" << std::endl;
            return -1;
        }
        else {
            std::cout << "DB Success" << std::endl;
            return 1;
        }
    }
};