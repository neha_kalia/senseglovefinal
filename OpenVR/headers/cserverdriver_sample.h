#ifndef CSERVERDRIVER_SAMPLE_H
#define CSERVERDRIVER_SAMPLE_H

#include "openvr_driver.h"
#include "csampledevicedriver.h"
#include "csamplecontrollerdriver.h"
#include "SenseGloveConnect.h"

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
class CServerDriver_Sample : public vr::IServerTrackedDeviceProvider
{
public:
    virtual vr::EVRInitError Init(vr::IVRDriverContext *pDriverContext);
    virtual void Cleanup();
    virtual const char *const *GetInterfaceVersions() { return vr::k_InterfaceVersions; }
    virtual void RunFrame();
    virtual bool ShouldBlockStandbyMode()  { return false; }
    virtual void EnterStandby()  {}
    virtual void LeaveStandby()  {}

private:
    SGCore::SG::SenseGlove glove;
    CSampleDeviceDriver *m_pNullHmdLatest = nullptr;
    CSampleControllerDriver<SGCore::SG::SenseGlove> *m_pController = nullptr;
    CSampleControllerDriver<SGCore::SG::SenseGlove> *m_pController2 = nullptr;
    SenseGloveConnect<SGCore::SG::SenseGlove>* connect;
};

#endif // CSERVERDRIVER_SAMPLE_H
