#include "Serialize.h"
#include <fstream>

/**
* Writes basic serialized data from testglove to file.
* More parameters can be added to the file if needed
*/
void Serialize::serialize(SGCore::SG::SenseGlove testGlove, int test_num) {
	std::fstream serialStream;
	serialStream.open("Serialized_Data.txt", std::fstream::app | std::fstream::out);
	serialStream << test_num << std::endl;
	std::cout << "in serialize" << std::endl;


	testGlove.SendHaptics(SGCore::Haptics::SG_BuzzCmd(0, 80, 0, 0, 0)); //vibrate the index fingerat 80% intensity.
	std::this_thread::sleep_for(std::chrono::milliseconds(200)); //vibrating for for 200ms.

	testGlove.SendHaptics(SGCore::Haptics::SG_BuzzCmd::off); //turn off all Buzz Motors.
	std::this_thread::sleep_for(std::chrono::milliseconds(10)); //wait for 10ms.

	//write sensordata to file
	bool gloveFound = SGCore::SG::SenseGlove::GetSenseGlove(testGlove);
	std::cout << "Glove Found in serialize" << gloveFound << std::endl;
	SGCore::SG::SG_SensorData sensorData;
	if (testGlove.GetSensorData(sensorData)) {
		std::cout << "in if statement 1" << std::endl;
		serialStream << sensorData.Serialize() << std::endl;
		
	}

	std::cout << "out if statement 1" << std::endl;
	//write glovepose to file
	SGCore::SG::SG_GlovePose glovePose;
	if (testGlove.GetGlovePose(glovePose))
	{
		serialStream << glovePose.Serialize() << std::endl;
		std::cout << "in if statement 2" << std::endl;
	}
	std::cout << "ou if statement 2" << std::endl;
	//write glove model to file
	SGCore::SG::SG_Model model = testGlove.GetGloveModel();
	serialStream << model.Serialize() << std::endl;

	serialStream << std::endl;
	serialStream.close();
}

/**
* Reads serialized data from file. 
* 
*/
bool Serialize::deserialize(int test_num, SGCore::SG::SG_Model& gloveModel, SGCore::SG::SG_SensorData& sensorData, SGCore::SG::SG_GlovePose& glovePose)
{
	std::ifstream inFile;
	std::string line;
	inFile.open("Serialized_Data.txt");
	std::cout << "Read Serialized_Data file" << std::endl;
	while (getline(inFile, line)) {
		std::cout << "read next line" << std::endl;
		if (std::stoi(line)==(test_num)) {
			std::cout << "found test case" << std::endl;
			getline(inFile, line);
			sensorData = SGCore::SG::SG_SensorData::Deserialize(line);
			getline(inFile, line);
			glovePose = SGCore::SG::SG_GlovePose::Deserialize(line);
			getline(inFile, line);
			gloveModel = SGCore::SG::SG_Model::Deserialize(line);
			getline(inFile, line);
			inFile.close();
			if (line.empty()) {
				return true;
			}
			else return false;
		}
		else {
			getline(inFile, line);
			getline(inFile, line);
			getline(inFile, line);
			getline(inFile, line);
		}
	}
	inFile.close();
	return false;
}


