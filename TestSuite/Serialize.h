#pragma once
#include <iostream>
#include "SenseGlove.h"

#include <thread>
#include <fstream>
#include <chrono>

class Serialize
{
public :
	//Writes serialized data to file
	static void serialize(SGCore::SG::SenseGlove testGlove, int test_num);
	//deserializes data from file based on test case number and calculates glovemodel, sensordata and glovepose
	static bool deserialize(int test_num, SGCore::SG::SG_Model& gloveModel, SGCore::SG::SG_SensorData& sensorData, SGCore::SG::SG_GlovePose& glovePose);
};

